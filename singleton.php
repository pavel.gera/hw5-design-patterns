<?php
class Singleton {
    private static $singleton;
    private function __construct() {}
    private function __clone() {}

    public static function newInstance() {
    if (self::$singleton === null) {
       self::$singleton = new Singleton();
        return self::$singleton;
    }
else return self::$singleton;
    }

    public function helloworld () {
        echo 'helloworld!';
    }



}

Singleton::newInstance()->helloworld();



