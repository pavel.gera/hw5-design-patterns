<?php
interface PaymentMethods {
    public static function TypeofPaymentSystem ($type);
}
class Visa implements PaymentMethods {
    public static function TypeofPaymentSystem($type) {
        echo 'Now you can pay with your Visa';
    }
}
class WebMoney implements PaymentMethods {
    public static function TypeofPaymentSystem($type)
    {
        echo 'Now you can pay from your Webmoney wallet';
    }
}
class BitCoin implements PaymentMethods {
    public static function TypeofPaymentSystem($type)
    {
        echo 'Now you can pay from your BitCoin wallet';
    }
}


class Strategy
{
    private $type;

    public function setType($type) {
        $this->type = $type;
        switch ($this->type) {
            case 'Visa' :
                return Visa::TypeofPaymentSystem($this->type);
            break;
            case 'WebMoney' :
                return WebMoney::TypeofPaymentSystem($this->type);
            break;
            case 'BitCoin' :
                return BitCoin::TypeofPaymentSystem($this->type);
            break;
            default:
                echo 'Please select type og payment system';
        }
        }

}

$payment = new Strategy();
$payment->setType('BitCoin');




